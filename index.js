const five = require("johnny-five");
const board = new five.Board();

const server = require('./scripts/server');
const robotArmHandler = require('./scripts/robot-arm-handler');

board.on("ready", function() {
    console.log("Connected");

    var robotArmConfig = {
        axis: {
            pivot: new five.Servo({
                pin: 0,
                address: 0x40,
                controller: "PCA9685",
                center: true,
                range: [0, 180]
            }),
            stand: new five.Servo({
                pin: 1,
                address: 0x40,
                controller: "PCA9685",
                center: true,
                range: [0, 180]
            }),
            shoulder: new five.Servo({
                pin: 2,
                address: 0x40,
                controller: "PCA9685",
                center: true,
                range: [0, 180]
            }),
            elbow: new five.Servo({
                pin: 3,
                address: 0x40,
                controller: "PCA9685",
                center: true,
                range: [0, 180]
            }),
            wrist: new five.Servo({
                pin: 4,
                address: 0x40,
                controller: "PCA9685",
                center: true,
                range: [0, 180]
            }),
            claw: new five.Servo({
                pin: 15,
                address: 0x40,
                controller: "PCA9685",
                center: true,
                range: [0, 180]
            })
        }
    };
    
    try {
        robotArmHandler.init(robotArmConfig);
    } catch (e) {
        console.error(e);
    };

    server.start();

    this.repl.inject({
        robotArmHandler,
        servos: robotArmConfig.axis,
        board   /* for board.io.reset */
    });
});