(function() {

    window.app = {
        sendRequest
    };

    var baseUrl = 'http://localhost:3010';
    var actionUrl = baseUrl + '/action';

    function sendRequest (config) {
        config = config || {};
        let params = config.params && config.params.length ? config.params.join('/') : '';
        const promise = new Promise(function(resolve, reject) { 
            let http = new XMLHttpRequest();
            let requestUrl = actionUrl + (params ? `/${params}` : '' );
            http.open("GET", requestUrl, true);
            http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            http.onreadystatechange = function() {
                console.log(http.responseText);
                if((http.readyState == 4 || http.readyState == 2) && http.status == 200) {
                    resolve(http);
                } else {
                    reject(http);
                }
            }
            http.send(null);
        });
        return promise;
    };

    var sendRequestDebounced = debounce(sendRequest, 200);

    var keys = {
        arrowUp: $('#key_arrow_up'),
        arrowDown: $('#key_arrow_down'),
        arrowLeft: $('#key_arrow_left'),
        arrowRight: $('#key_arrow_right'),
        a: $('#key_a'),
        d: $('#key_d'),
        q: $('#key_q'),
        e: $('#key_e'),
        w: $('#key_w'),
        s: $('#key_s'),
        ctrl: $('#key_ctrl'),
        space: $('#key_space'),
    };

    window.addEventListener('keydown', function(keypressEvent) {
        console.log('keyCode: ' + keypressEvent.keyCode);

        // Stand: ArrowUp - ArrowDown
        if (keypressEvent.keyCode === 38) {
            sendRequestDebounced({  params: ['stand', true]  });
            keys.arrowUp.show();
        }
        if (keypressEvent.keyCode === 40) {
            sendRequestDebounced({  params: ['stand', false]  });
            keys.arrowDown.show();
        }

        // Pivot: ArrowLeft - ArrowRight
        if (keypressEvent.keyCode === 37) {
            sendRequestDebounced({ params: ['pivot', true] });
            keys.arrowLeft.show();
        }
        if (keypressEvent.keyCode === 39) {
            sendRequestDebounced({ params: ['pivot', false] });
            keys.arrowRight.show();
        }

        // Shoulder: a - d
        if (keypressEvent.keyCode === 65) {
            sendRequestDebounced({ params: ['shoulder', false] });
            keys.a.show();
             
        }
        if (keypressEvent.keyCode === 68) {
            sendRequestDebounced({ params: ['shoulder', true] });
            keys.d.show();
        }

        // Elbow: q - e
        if (keypressEvent.keyCode === 81) {
            sendRequestDebounced({ params: ['elbow', false] });
            keys.q.show();
        }
        if (keypressEvent.keyCode === 69) {
            sendRequestDebounced({ params: ['elbow', true] });
            keys.e.show();
        }

        // Wrist: w - s
        if (keypressEvent.keyCode === 87) {
            sendRequestDebounced({ params: ['wrist', false] });
            keys.w.show();
        }
        if (keypressEvent.keyCode === 83) {
            sendRequestDebounced({ params: ['wrist', true] });
            keys.s.show();
        }

        // Claw: left ctrl + space
        if (keypressEvent.keyCode === 32) {
            sendRequestDebounced({ params: ['claw', true] });
            keys.space.show();
        }
        if (keypressEvent.keyCode === 17) {
            sendRequestDebounced({ params: ['claw', false] });
            keys.ctrl.show();
        }
    });

    window.addEventListener('keyup', function(keypressEvent) {
        
        // Stand: ArrowUp - ArrowDown
        if (keypressEvent.keyCode === 38) {
            keys.arrowUp.hide();
        }
        if (keypressEvent.keyCode === 40) {
            keys.arrowDown.hide();
        }

        // Pivot: ArrowLeft - ArrowRight
        if (keypressEvent.keyCode === 37) {
            keys.arrowLeft.hide();
        }
        if (keypressEvent.keyCode === 39) {
            keys.arrowRight.hide();
        }

        // Shoulder: a - d
        if (keypressEvent.keyCode === 65) {
            keys.a.hide();
        }
        if (keypressEvent.keyCode === 68) {
            keys.d.hide();
        }

        // Elbow: q - e
        if (keypressEvent.keyCode === 81) {
            keys.q.hide();
        }
        if (keypressEvent.keyCode === 69) {
            keys.e.hide();
        }

        // Wrist: w - s
        if (keypressEvent.keyCode === 87) {
            keys.w.hide();
        }
        if (keypressEvent.keyCode === 83) {
            keys.s.hide();
        }

        // Claw: left ctrl + space
        if (keypressEvent.keyCode === 32) {
            keys.space.hide();
        }
        if (keypressEvent.keyCode === 17) {
            keys.ctrl.hide();
        }
    });

    function debounce(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };
})();