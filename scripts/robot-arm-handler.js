const five = require('johnny-five');
const RobotArm = require('robotarm');

var exports = module.exports = {};
Object.assign(exports, {
    init,
    move
});

var robotArm = {};
var stepTime = 100;
var angleStep = 10;

function init(robotArmConfig) {
    
    if (!robotArmConfig || !robotArmConfig.axis) {
        throw new Error('Missing init configuration for the RobotArm!');
    };
    
    if (
        !(robotArmConfig.axis.pivot instanceof five.Servo) || 
        !(robotArmConfig.axis.stand instanceof five.Servo) || 
        !(robotArmConfig.axis.shoulder instanceof five.Servo) ||
        !(robotArmConfig.axis.elbow instanceof five.Servo) ||
        !(robotArmConfig.axis.wrist instanceof five.Servo) ||
        !(robotArmConfig.axis.claw instanceof five.Servo)
    ) {
        throw new Error('Missing axis configuration for the RobotArm!');
    };
    
    robotArm = new RobotArm(robotArmConfig);
    
    // // Init lockers
    // robotArm.axis.pivot.locked = false;
    // robotArm.axis.stand.locked = false;
    // robotArm.axis.shoulder.locked = false;
    // robotArm.axis.elbow.locked = false;
    // robotArm.axis.wrist.locked = false;
    // robotArm.axis.claw.locked = false;
}

function move(robotArmAxis, clockWise) {
    console.log('moving '+ robotArmAxis +' with clockwise flag ' + clockWise);
    let promise = new Promise(function(resolve, reject) { 
        //if (!robotArm.axis[robotArmAxis].locked) {
            //robotArm.axis[robotArmAxis].locked = true;
            let pos
            if (clockWise) {
                pos = robotArm.axis[robotArmAxis].position + angleStep;
                console.log('robotArmAxis: clockWise: ' + pos);
            } else {
                console.log('robotArmAxis: counterClockWise: ' + pos);
                pos = robotArm.axis[robotArmAxis].position - angleStep;
            }
            console.log('pos: ' + pos);
            if ( pos <= 180 && pos >= 0) {
                try {
                    console.log('moving to position ' + pos);
                    robotArm.axis[robotArmAxis].to( pos, stepTime );
                    setTimeout(() => { 
                        console.log('resolve');
                        // robotArm.axis[robotArmAxis].locked = false; 
                        resolve({pos: robotArm.axis[robotArmAxis].position});
                    }, stepTime);
                } catch (e) {
                    // robotArm.axis[robotArmAxis].locked = false;
                    reject(e);
                }
            }
        // } else {
        //     console.log('axis locked!');
        // }
    });
    return promise;
}

function resetIO() {
    // TODO implement pin resets
}
/*
function move(action) {
    switch (action) {

        case 'PivotUp':
            returnprocessMove('pivot', true);
            break;
        case 'PivotDown':
            processMove('pivot', false);
            break;

        case 'StandUp':
            processMove('stand', true);
            break;
        case 'StandDown':
            processMove('stand', false);
            break;

        case 'ShoulderUp':
            processMove('shoulder', true);
            break;
        case 'ShoulderDown':
            processMove('shoulder', false);
            break;

        case 'ElbowUp':
            processMove('elbow', true);
            break;
        case 'ElbowDown':
            processMove('elbow', false);
            break;

        case 'WristUp':
            processMove('wrist', true);
            break;
        case 'WristDown':
            processMove('wrist', false);
            break;

        case 'ClawUp':
            processMove('claw', true);
            break;
        case 'ClawDown':
            processMove('claw', false);
            break;                
    };
}
*/
