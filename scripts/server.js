const express = require('express');
const app = express();
const path = require("path");
//const bodyParser = require('body-parser');
const roboArmHandler = require('./robot-arm-handler');

var exports = module.exports = {};
Object.assign(exports, {
    start
});

/*app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname+'/../html/index.html'));
})*/

app.get('/action/:action/:clickWise', function (req, res) {
	console.log(req.params);
	return roboArmHandler.move(req.params['action'], req.params['clickWise'] == 'true')
		.then(
			(response) => {
				res.status(200).send(response);
			}, (reason) => {
				res.status(500).send(reason);
			}
		);
});
app.get('/action', function (req, res) {
    res.status(404);
})

function start() {
    //app.use(bodyParser.urlencoded({ extended: true }));
    app.use(express.static('./public'));
    app.listen(3010);
    console.log('App listening on localhost:3010');
}