# Robot Servos Arm

Implementation for ruling the [6 DOF Robot Arm](https://www.aliexpress.com/item-img/Arduino-Robot-6-DOF-Aluminium-Clamp-Claw-Mount-Kit-Mechanical-Robotic-Arm-Servos-Metal-Servo-Horn/32692963362.html "AliExpress link"). Using [Johnny-Five](http://johnny-five.io), a JavaScript robotics platform.

![6 DOF Robot Arm](https://ae01.alicdn.com/kf/HTB1Oq6vKFXXXXXSaXXXq6xXFXXXe/Arduino-Robot-6-DOF-Aluminium-Clamp-Claw-Mount-Kit-Mechanical-Robotic-Arm-Servos-Metal-Servo-Horn.jpg "The Arm")

## Hardware
- Arduino Uno
- PCA9685 servo shield
- 6 x Servos 


